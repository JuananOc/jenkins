# Install

Jenkins installation process: https://jenkins.io/doc/book/installing/


# Requirements

## Gitlab

In order to exeute the full example you need to configure your private and public keys to access into jenkins

1. Create an SSH key pair to your projects

2. Access into https://gitlab.com/profile/keys and add your ssh id_rsa.pub files

# Descriptions

1. Inside docker folder we have a simple jenkins setup configured with all the configuration manage as a code inside a Jenkins container

2. (PENDING) Inside docker-compose folder we have a jenkins master communicating with a Jenkins agent, with the previous configuration generated in the docker for the jenkins agent

> Watch internal README.md of the folders to learn how to use it
