# Jenkins in docker-compose

# Setup

1. Starts jenkins master, configured for starts all the agents:

```bash
cd master/
source run-master.sh
```

2. Starts jenkins agents, configured for connecting into jenkins:

```bash
cd ../agents/
./run-agents.sh
```

# Cleanup process

Because networks from docker composed are coupled in order to jenkins agents to connect into jenkins master, we need to execute the cleanup.sh scripts in order.

```bash
cd agents/
./cleanup.sh

cd ../master
./cleanup.sh
```

# Add a new agent type

> Pending to be automated.

To Add a new agent type you need to follow the next steps:

1. In `master/build/jenkins.yaml` you need to add a new node, changing the parameters `labelString` and `name` to the agent name.

2. In agent:

* Copy the directory of raw agent and rename it to the agent name

* In `agents/docker-compose.yaml` add a new section inside services to the new agent changing:
    * service-name to `jenkins-<agent name>-agent`.
    * service-name.build.context pointing to the directory previously created
    * service-name.args.agent_name to agent name
    * service-name.args.agent_secret environment variable should change to JENKINS_<AGENT_NAME>_SECRET

* In `agents/run-agents.sh` you need to add the secret here because we can not get this value dinamically from the docker-compose.yml, copy one of the existing `JENKINS_<AGENT_NAME>_SECRET` to the new agent, changing in the url the name of the previous AGENT name into the new one

* Needs to create the Dockerfile with the agent configuration required. Watch `Create Dockerfile for agents`

## Create Dockerfile for agents

For creating this, Create a multistage Docker file to generate the binaries or configuration specific of your new agent and Copy to the jenkins inbound image.
