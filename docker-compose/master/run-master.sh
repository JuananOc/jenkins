#!/usr/bin/env bash

# 1. Ensure that is deleted
docker-compose down

# 2. Build the image
docker-compose build --no-cache

# 3. Launch the image

# To pass secrets that are no literals into Docker compose we are using the following reference
# https://stackoverflow.com/questions/58622131/docker-compose-setting-environment-variables-that-are-not-literals
JENKINS_GITLAB_SSH=$(cat $JENKINS_JCASCCONFIG/jenkins_id_rsa) \
    docker-compose up --force-recreate -d
