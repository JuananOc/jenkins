#!/usr/bin/env bash

AGENT_WORKDIR="/home/jenkins/agent"

# 1. Ensure that is deleted
docker-compose down


# 2. Build the image

# TODO: Retrieve this information from docker compose
# This points into localhost:8080 because is the url exposed in my local machine to this jenkins
JENKINS_RAW_SECRET=$(curl "http://localhost:8080/computer/raw/slave-agent.jnlp" | xmllint --xpath "string(/jnlp/application-desc/argument[1])" - ) \
JENKINS_TERRAGRUNT_SECRET=$(curl "http://localhost:8080/computer/terragrunt/slave-agent.jnlp" | xmllint --xpath "string(/jnlp/application-desc/argument[1])" - ) \
JENKINS_AGENT_WORKDIR=$AGENT_WORKDIR \
docker-compose build --no-cache


# 3. Launch the image
docker-compose up --force-recreate -d
