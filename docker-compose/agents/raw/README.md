# Jenkins in Docker

To setup this custom Jenkins in Docker just need to perform two actions:

1. Execute the `build.sh` to create the image based in the Jenkins JcasC plugin and all the plugins listed.

```bash
./build.sh
```

2. Execute `run.sh` to run the image in this terminal this is used for quick testing inside the platform.

```bash
./run.sh
```

3. To cleanup all the project, execute:

```bash
./cleanup.sh
```

# How to add new plugins

Plugins are located inside `plugins.txt`. Just adding here the plugin and the version is all that you need. 
More information inside https://github.com/jenkinsci/docker#preinstalling-plugins

Process to update plugins is:

```bash
# Update plugins file

# Rebuild the image
./build.sh

# Re launch Jenkins
./run.sh
```

# How to update Jenkins configuration

Jenkins configuration is all located inside `jenkins.yaml`. 
Here we will store al the configuration that will be loaded.

Process to update Jenkins is:

```bash
# Update jenkins.yaml file

# Rebuild the image
./build.sh

# Re launch Jenkins
./run.sh
```

# How to pass environment variables to Jenkins configuration

For example, in `jenkins.yaml` we use the variable `JENKINS_GITLAB_SSH`. 
To pass this variables:

1. In the configuration files we put the configuration making reference to the environment variable.

2. In the run.sh script we pass environment variable.
   In this case a private key getting the content of the environment variable outside the repository (it's sensible credential)

3. Enjoy!

# References

* Jenkins image configuration: https://github.com/jenkinsci/docker#passing-jenkins-launcher-parameters
* Jenkins Jcasc: https://www.jenkins.io/projects/jcasc/
* Jeknins Jcasc github repository: https://github.com/jenkinsci/configuration-as-code-plugin/blob/master/README.md
