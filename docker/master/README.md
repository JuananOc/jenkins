# Jenkins in Docker

TLDR:

0. See all the actions:

```bash
make help
```

1. Setup the process:

```bash
make env > .envrc
direnv allow
make setup
```

2. Clean

```bash
make clean
```

`make setup` Under the hood is:

1. Creating the image based in the `Dockerfile`.

```bash
make build
```

2. Running the image in background and expose the ports with the host.

```bash
make run
```

# How to add new plugins

Plugins are located inside `plugins.txt`. Just adding here the plugin and the version is all that you need. 
More information inside https://github.com/jenkinsci/docker#preinstalling-plugins

Process to update plugins is:

```bash
# Update plugins file

make setup
```

# How to update Jenkins configuration

Jenkins configuration is all located inside `jenkins.yaml`.
Here we will store al the configuration that will be loaded.

Process to update Jenkins is:

```bash
# Update jenkins.yaml file

make setup
```

# How to pass environment variables to Jenkins configuration

For example, in `jenkins.yaml` we use the variable `JENKINS_GITLAB_SSH`.
To pass this variables:

1. In the configuration files we put the configuration making reference to the environment variable.

2. In the `Makefile` script we pass environment variable.
   In this case a private key getting the content of the environment variable outside the repository (it's sensible credential)

3. Enjoy!

# References

* Jenkins image configuration: https://github.com/jenkinsci/docker#passing-jenkins-launcher-parameters
* Jenkins Jcasc: https://www.jenkins.io/projects/jcasc/
* Jeknins Jcasc github repository: https://github.com/jenkinsci/configuration-as-code-plugin/blob/master/README.md
